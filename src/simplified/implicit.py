#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# Parameters

dt = 1
theta0 = 1
phi0 = 0
T = 4 * np.pi

# Set up

N = int(T / dt + 1)
t = np.linspace(0, T, N)
theta = np.zeros(np.shape(t))
phi = np.zeros(np.shape(t))
theta[0] = theta0
phi[0] = phi0

# Compute

for i in range(N - 1):
    theta[i + 1] = (phi[i] * dt + theta[i]) / (1 + dt ** 2)
    phi[i + 1] = (theta[i + 1] - theta[i]) / dt

# Plot

plt.figure()
plt.plot(t, theta, t, phi)
plt.xlabel('t')
plt.ylabel('Angle and angular velocity of pendulum')
plt.legend(('angle', 'angular velocity'))
plt.title("Implicit Euler")
plt.show()
