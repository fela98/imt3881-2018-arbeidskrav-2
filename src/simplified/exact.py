#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# Parameters

dt = 1/1000
T = 4 * np.pi

# Set up

N = int(T / dt + 1)
t = np.linspace(0, T, N)

# Plot

plt.figure()
plt.plot(t, np.cos(t), t, -np.sin(t))
plt.xlabel('t')
plt.ylabel('Angle and angular velocity of pendulum')
plt.legend(('angle', 'angular velocity'))
plt.title('Exact')
plt.show()
