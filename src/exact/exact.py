#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# Parameters

dt = 1/1000
theta0 = 3
phi0 = 0
T = 8 * np.pi

# Set up

N = int(T / dt + 1)
t = np.linspace(0, T, N)


# Compute
def explicit():
    theta = np.zeros(np.shape(t))
    phi = np.zeros(np.shape(t))
    theta[0] = theta0
    phi[0] = phi0

    for i in range(N - 1):
        theta[i + 1] = theta[i] + phi[i] * dt
        phi[i + 1] = phi[i] - dt * np.sin(theta[i])
        if phi[i] > 0 > phi[i + 1]:
            print('Period: ' + str(i * dt))

    return theta


def heun():
    theta = np.zeros(np.shape(t))
    phi = np.zeros(np.shape(t))
    theta[0] = theta0
    phi[0] = phi0

    for i in range(N - 1):
        theta[i + 1] = theta[i] + (1/2)*(2 + dt) * phi[i] * dt
        phi[i + 1] = -(1/2) * dt * (np.sin(theta[i]) + np.sin(theta[i] - dt * np.sin(theta[i]))) + phi[i]
        if phi[i] > 0 > phi[i + 1]:
            print('Period: ' + str(i * dt))

    return theta


# Plot

plt.figure()
plt.plot(t, explicit(), t, heun())
plt.xlabel('t')
plt.ylabel('Angle')
plt.legend(('explicit', 'heun'))
plt.title("")
plt.show()
